\header {
  copyrightYear = "2009"
  composer = "George Frideric Handel"
  opus = "HWV 56"
  date = "1741"
}

%% LilyPond options:
#(ly:set-option 'original-layout #f)
#(ly:set-option 'use-ancient-clef #f)
#(ly:set-option 'show-ancient-clef #f)
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
%% Staff size
#(set-global-staff-size
  (cond (;; parties vocales avec réduction clavier
         (eqv? (ly:get-option 'part) 'reduction) 18)
        (;; chœur
         (eqv? (ly:get-option 'part) 'choir) 20)
        (;; parties séparées
         (symbol? (ly:get-option 'part)) 20)
        (;; conducteur
         else 16)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (cond (;; parties vocales : optimal
            (memq (ly:get-option 'part) '(reduction choir))
            ly:optimal-breaking)
           (;; parties séparées instrumentales : page-turn
            (symbol? (ly:get-option 'part))
            ly:page-turn-breaking)
           (;; conducteur : optimal
            else
            ly:optimal-breaking)))
}

\language "italiano"
\include "nenuvar-lib.ily"
\include "nenuvar-garamond.ily"
\setPath "."
opusTitle =
#(define-music-function (parser location title) (string?)
   (*opus-title* title)
   (make-music 'Music 'void #t))

\opusTitle "Messiah"

\paper {
  tocTitle = "CONTENTS"
}
\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'inside 
  }
}


\opusPartSpecs
#`((violino1 "Violino I" () (#:notes "violino1"))
   (violino2 "Violino II" () (#:notes "violino2"))
   (viola "Viola" () (#:notes "viola" #:clef "alto"))
   (bassi "Bassi" () (#:notes "bassi" #:clef "bass" #:score-template "score-bassi"))

   (oboe "Oboe I II" () (#:notes "oboe"))
   (trombe-timpani "Trombe, Timpani" () (#:score "score-tt"))

   (choir "Choir" () (#:score-template "score-vocal"))
   (reduction "Voice and keyboard reduction" () (#:score-template "score-vocal-keyboard")))

part=
#(define-music-function (parser location act-title) (string?)
  (increase-rehearsal-major-number)
  (add-page-break parser)
  (add-toc-item parser 'tocActMarkup act-title)
  (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
  (*act-title* act-title)
  (add-odd-page-header-text
    parser
    (format #f "~a" (string-upper-case (*act-title*)))
    #f)
  (add-toplevel-markup parser
    (markup #:act (string-upper-case act-title)))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))

partNoBreak=
#(define-music-function (parser location act-title) (string?)
  (increase-rehearsal-major-number)
  (add-toc-item parser 'tocActMarkup act-title)
  (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
  (*act-title* act-title)
  (add-odd-page-header-text
    parser
    (format #f "~a" (string-upper-case (*act-title*)))
    #f)
  (add-toplevel-markup parser
    (markup #:act (string-upper-case act-title)))
  (add-no-page-break parser)
  (make-music 'Music 'void #t))

trill = #(make-articulation "trill")

applyDurations =
#(define-music-function (parser location pattern music) (ly:music? ly:music?)
  "\\applyDurations { c'16. c32 } { c d e f }
==>
{ c16. d32 e16. f32 }"
   (let ((durations (apply
                     circular-list
                     (let ((result (list)))
                       (music-map
                        (lambda (event)
                          (if (eqv? (ly:music-property event 'name) 'NoteEvent)
                              (set! result (cons (ly:music-property event 'duration) result)))
                          event)
                        pattern)
                       (reverse! result)))))
     (music-map
      (lambda (event)
        (cond ((eqv? (ly:music-property event 'name) 'NoteEvent)
               (set! (ly:music-property event 'duration) (car durations))
               (set! durations (cdr durations))))
        event)
      music)))
 
fineMark = \endMark "Fine."


%%%
%% double pointée triple x2
dpts =
#(define-music-function (parser location chords) (ly:music?)
   (define (make-16.-32-16.-32 chord)
     (let ((pitches '()))
       (music-map (lambda (m)
                    (if (eqv? (ly:music-property m 'name) 'NoteEvent)
                        (set! pitches (cons (ly:music-property m 'pitch) pitches)))
                    m)
                  chord)
       (let ((chord16. (make-music 'EventChord
                         'elements (map (lambda (pitch)
                                          (make-music 'NoteEvent
                                           'duration (ly:make-duration 4 1)
                                           'pitch pitch))
                                        pitches)))
             (chord32  (make-music 'EventChord
                         'elements (map (lambda (pitch)
                                          (make-music 'NoteEvent
                                           'duration (ly:make-duration 5)
                                           'pitch pitch))
                                        pitches))))
         (make-music 'SequentialMusic 'elements (list chord16. chord32 chord16. chord32)))))
   (make-music 'SequentialMusic
               'elements (map make-16.-32-16.-32 (ly:music-property chords 'elements))))

upup = { \change Staff = "up" \oneVoice }
downup = { \change Staff = "up" \voiceTwo }
updown = { \change Staff = "down" \voiceOne }
downdown = { \change Staff = "down" \oneVoice }

#(define-markup-command (text-column props layout columns) (markup-list?)
   (interpret-markup props layout #{\markup\abs-fontsize#10 \column  \with-line-width-ratio#0.8 { $columns \null } #}))
