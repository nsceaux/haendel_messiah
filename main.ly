\version "2.23.4"
\include "common.ily"
\include "title.ily"

%%%
%%% PART I
%%%
\bookpart {
  \part "Part I"
  %% n°1
  \pieceToc "Sinfonia"
  \includeScore "part1/1-sinfonia"
}
\bookpart {
  %% n°2
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Comfort ye, my people } (tenor)
  }
  \includeScore "part1/2-recit-ComfortYeMyPeople"
}
\bookpart {
  %% n°3
  \pieceToc\markup\wordwrap {
    Air: \italic { Every valley shall be exalted } (tenor)
  }
  \includeScore "part1/3-air-EveryValleyShallBeExalted"
}
\bookpart {
  %% n°4
  \pieceToc\markup\wordwrap {
    Chorus: \italic { And the glory of the Lord }
  }
  \includeScore "part1/4-chorus-AndTheGloryOfTheLord"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% n°5
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Thus saith the Lord of Hosts } (bass)
  }
  \includeScore "part1/5-recit-ThusSaithTheLordOfHosts"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% n°6
  \pieceToc\markup\wordwrap {
    Air: \italic { But who may abide the day of His coming } (contr'alto)
  }
  \includeScore "part1/6-air-ButWhoMaAbideTheDayOfHiscoming"
}
\bookpart {
  %% n°7
  \pieceToc\markup\wordwrap {
    Chorus: \italic { And He shall purify the sons of Levi }
  }
  \includeScore "part1/7-chorus-AndHeShallPurifyTheSonsOfLevi"
}
\bookpart {
  \paper { systems-per-page = 5 }
  %% n°8
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Behold, a virgin shall conceive } (contr'alto)
  }
  \includeScore "part1/8-recit-BeholdAVirginShallConceive"
  %% n°9
  \pieceToc\markup\wordwrap {
    Air, chorus: \italic { O thou that tellest good tidings to Zion } (contr'alto, chorus)
  }
  \includeScore "part1/9-air-OThouThatTellestGoodTidingsToZion"
}
\bookpart {
  \includeScore "part1/9-chorus-OThouThatTellestGoodTidingsToZion"
}
\bookpart {
  %% n°10
  \pieceToc\markup\wordwrap {
    Recitative: \italic { For, behold! darkness shall cover the earth } (bass)
  }
  \includeScore "part1/10-recit-ForBeholdDarknessShallCoverTheEarth"
}
\bookpart {
  %% n°11
  \pieceToc\markup\wordwrap {
    Air: \italic { The people that walked in darkness } (bass)
  }
  \includeScore "part1/11-air-ThePeopleThatWalkedInDarkness"
}
\bookpart {
  %% n°12
  \pieceToc\markup\wordwrap {
    Chorus: \italic { For unto us a child is born }
  }
  \includeScore "part1/12-chorus-ForUntoUsAChildIsBorn"
}
\bookpart {
  %% n°13
  \pieceToc "Pifa"
  \includeScore "part1/13-pifa"
}
\bookpart {
  %% n°14
  \pieceToc\markup\wordwrap {
    Recitative: \italic { There were shepherds abiding in the fields } (soprano)
  }
  \includeScore "part1/14-recit-ThereWereShepherdsAbidingInTheFields"
  \includeScore "part1/14-recit-AndLoTheAnGelOfTheLordCameUpOnThem"
  %% n°15
  \pieceToc\markup\wordwrap {
    Recitative: \italic { And the angel said unto them } (soprano)
  }
  \includeScore "part1/15-recit-AndTheAngelSaidUntoThem"
  %% n°16
  \pieceToc\markup\wordwrap {
    Recitative: \italic { And suddenly there was with the angel } (soprano)
  }
  \includeScore "part1/16-recit-AndSuddenlyThereWasWithTheAngel"
}
\bookpart {
  \paper { systems-per-page = 1 }
  %% n°17
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Glory to God in the highest }
  }
  \includeScore "part1/17-chorus-GloryToGodInTheHighest"
}
\bookpart {
  \paper { systems-per-page = 5 }
  %% n°18
  \pieceToc\markup\wordwrap {
    Air: \italic { Rejoice greatly, O daughter of Zion } (soprano)
  }
  \includeScore "part1/18-air-RejoiceGreatlyODaughterOfZion"
}
\bookpart {
  %% n°19
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Then shall the eyes of the blind } (alto)
  }
  \includeScore "part1/19-recit-ThenShallTheEyesOfTheBlind"
  %% n°20
  \pieceToc\markup\wordwrap {
    Air: \italic { He shall feed His flock like a shepherd } (alto, soprano)
  }
  \includeScore "part1/20-air-HeShallFeedHisFlockLikeAShepherd"
}
\bookpart {
  %% n°21
  \pieceToc\markup\wordwrap {
    Chorus: \italic { His yoke is easy }
  }
  \includeScore "part1/21-chorus-HisYokeIsEasy"
  \actEnd\markup { END OF THE FIRST PART }
}

%%%
%%% PART II
%%%
\bookpart {
  \part "Part II"
  %% n°1
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Behold the Lamb of God! }
  }
  \includeScore "part2/1-chorus-BeholdTheLambOfGod"
}
\bookpart {
  %% n°2
  \pieceToc\markup\wordwrap {
    Air: \italic { He was despised and rejected } (alto)
  }
  \includeScore "part2/2-air-HeWasDespisedAndRejected"
}
\bookpart {
  %% n°3
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Surely He hath borne our griefs }
  }
  \includeScore "part2/3-chorus-SurelyHeHathBorneOurGriefs"
  \includeScore "part2/3-chorus-AndWithHisStripesWeAreHealed"
}
\bookpart {
  %% n°4
  \pieceToc\markup\wordwrap {
    Chorus: \italic { All we like sheep have gone astray }
  }
  \includeScore "part2/4-chorus-AllWeLikeSheepHaveGoneAstray"
}
\bookpart {
  %% n°5
  \pieceToc\markup\wordwrap {
    Recitative: \italic { All they that see Him laugh him to scorn } (tenor)
  }
  \includeScore "part2/5-recit-AllTheyThatSeeHimLaughHimToScorn"
  %% n°6
  \pieceToc\markup\wordwrap {
    Chorus: \italic { He trusted in God }
  }
  \includeScore "part2/6-chorus-HeTrustedInGod"
}
\bookpart {
  %% n°7
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Thy rebuke hath broken His heart } (tenor)
  }
  \includeScore "part2/7-recit-ThyRebukeHathBrokenHisHeart"
  %% n°8
  \pieceToc\markup\wordwrap {
    Air: \italic { Behold, and see if there be any sorrow } (tenor)
  }
  \includeScore "part2/8-air-BeholdAndSeeIfThereBeAnySorrow"
  %% n°9
  \pieceToc\markup\wordwrap {
    Recitative: \italic { He was cut off out of the land of the living } (tenor)
  }
  \includeScore "part2/9-recit-HeWasCutOffOutOfTheLandOfTheLiving"
  %% n°10
  \pieceToc\markup\wordwrap {
    Air: \italic { But thou didst not leave His soul in hell } (tenor)
  }
  \includeScore "part2/10-air-ButThouDidstNotLeaveHisSoulInHell"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% n°11
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Lift up your heads, O ye gates }
  }
  \includeScore "part2/11-chorus-LiftUpYourHeadsOYeGates"
}
\bookpart {
  %% n°12
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Unto which of the angels said He at any time } (tenor)
  }
  \includeScore "part2/12-recit-UntoWhichOfTheAngelsSaidHeAtAnyTime"
  %% n°13
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Let all the angels of God worship Him }
  }
  \includeScore "part2/13-chorus-LetAllTheAngelsOfGodWorshipHim"
}
\bookpart {
  %% n°14
  \pieceToc\markup\wordwrap {
    Air: \italic { Thou art gone up on high } (alto)
  }
  \includeScore "part2/14-air-ThouArtGoneUpOnHigh"
}
\bookpart {
  %% n°15
  \pieceToc\markup\wordwrap {
    Chorus: \italic { The Lord gave the word }
  }
  \includeScore "part2/15-chorus-TheLordGaveTheWord"
}
\bookpart {
  %% n°16
  \pieceToc\markup\wordwrap {
    Air: \italic { How beautiful are the feet } (soprano)
  }
  \includeScore "part2/16-air-HowBeautifulAreTheFeet"
}
\bookpart {
  %% n°17
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Their sound is gone out into all lands }
  }
  \includeScore "part2/17-chorus-TheirSoundIsGoneOutIntoAllLands"
}
\bookpart {
 \paper { systems-per-page = 3 }
  %% n°18
  \pieceToc\markup\wordwrap {
    Air: \italic { Why do the nations so furiously rage together } (bass)
  }
  \includeScore "part2/18-air-WhyDoTheNationsSoFuriouslyRageTogether"
}
\bookpart {
  %% n°19
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Let us break their bonds asunder }
  }
  \includeScore "part2/19-chorus-LetUsBreakTheirBondsAsunder"
}
\bookpart {
  %% n°20
  \pieceToc\markup\wordwrap {
    Recitative: \italic { He that dwelleth in heaven } (tenor)
  }
  \includeScore "part2/20-recit-HeThatDwellethInHeaven"
  %% n°21
  \pieceToc\markup\wordwrap {
    Air: \italic { Thou shalt break them with a rod of iron } (tenor)
  }
  \includeScore "part2/21-air-ThouShaltBreakThemWithARodOfIron"
}
\bookpart {
  %% n°22
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Hallelujah }
  }
  \includeScore "part2/22-chorus-Hallelujah"
  \actEnd \markup { END OF THE SECOND PART }
}

%%%
%%% PART III
%%%
\bookpart {
  \part "Part III"
  %% n°1
  \pieceToc\markup\wordwrap {
    Air: \italic { I know that my Redeemer liveth } (soprano)
  }
  \includeScore "part3/1-air-IKnowThatMyRedeemerLiveth"
}
\bookpart {
  %% n°2
  \pieceToc\markup\wordwrap {
    Chorus and soli: \italic { Since by man came death }
  }
  \includeScore "part3/2-chorus-SinceByManCameDeath"
  %% n°3
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Behold, I tell you a mistery } (bass)
  }
  \includeScore "part3/3-recit-BeholdITellYouAMistery"
}
\bookpart {
  %% n°4
  \pieceToc\markup\wordwrap {
    Air: \italic { The trumpet shall sound } (bass)
  }
  \includeScore "part3/4-air-TheTrumpeShallSound"
  %% n°5
  \pieceToc\markup\wordwrap {
    Recitative: \italic { Then shall be brought to pass } (contr'alto)
  }
  \includeScore "part3/5-recit-ThenShallBeBroughtToPass"
  %% n°6
  \pieceToc\markup\wordwrap {
    Duet: \italic { O death! where is thy sting? } (contr'alto and tenor)
  }
  \includeScore "part3/6-duet-ODeathWhereIsThySting"
}
\bookpart {
  %% n°7
  \pieceToc\markup\wordwrap {
    Chorus: \italic { But thanks be to God }
  }
  \includeScore "part3/7-chorus-ButThanksBeToGod"
}
\bookpart {
  %% n°8
  \pieceToc\markup\wordwrap {
    Air: \italic { If God be for us } (soprano)
  }
  \includeScore "part3/8-air-IfGodBeForUs"
}
\bookpart {
  \paper { systems-per-page = 1 }
  %% n°9
  \pieceToc\markup\wordwrap {
    Chorus: \italic { Worthy is the Lamb }
  }
  \includeScore "part3/9-chorus-WorthyIsTheLamb"
}
\bookpart {
  \paper { systems-per-page = 1 }
  %% n°10
  \pieceToc\markup\wordwrap { Chorus: \italic { Amen } }
  \includeScore "part3/10-chorus-Amen"
  \actEnd \markup { END OF THE ORATORIO }
}
