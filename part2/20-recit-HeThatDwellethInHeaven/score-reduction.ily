\score {
  \new ChoirStaff <<
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout {
    system-count = 2
    indent = \largeindent
  }
  \midi { }
}