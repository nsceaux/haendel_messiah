\piecePartSpecs
#`((bassi #:score-template "score-voix-figures")
   (reduction #:score "score-reduction")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    He that dwelleth in the heavens shall laugh them to scorn;
    the Lord shall have them in derision.
  }
}#}))
