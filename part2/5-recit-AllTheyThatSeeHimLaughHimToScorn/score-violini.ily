\score {
  <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withTinyLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "lyrics"
    \new GrandStaff <<
      \new Staff \with { instrumentName = "Violino I II" } <<
        \global \clef "treble" \includeNotes "violino12"
      >>
      \new Staff \with { instrumentName = "Violino III" } <<
        \global \clef "treble" \includeNotes "violino3"
      >>
    >>
  >>
  \layout { indent = \largeindent }
}
