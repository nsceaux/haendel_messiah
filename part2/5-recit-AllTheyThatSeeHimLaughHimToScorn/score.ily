\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff \with { instrumentName = "Violino I II" } <<
          \global \clef "treble" \includeNotes "violino12"
        >>
        \new Staff \with { instrumentName = "Violino III" } <<
          \global \clef "treble" \includeNotes "violino3"
        >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \clef "alto" \includeNotes "viola"
      >>
    >>
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
