\piecePartSpecs
#`((violino1 #:score "score-violini")
   (violino2 #:score "score-violini")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix-figures")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    All they that see him laugh him to scorn:
    they shoot out their lips, they shake their heads, saying:
  }
}#}))
