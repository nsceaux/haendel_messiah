\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi #:clef "treble")
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Let us break their bonds asunder, and cast away their yokes from us.
  }
}#}))
