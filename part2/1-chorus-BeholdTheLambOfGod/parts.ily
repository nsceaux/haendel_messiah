\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (oboe #:notes "vcanto")
   (bassi)
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Behold the Lamb of God, that taketh away the sins of the world.
  }
}#}))
