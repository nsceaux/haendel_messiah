\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix-figures")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    He was cut off out of the land of the living:
    for the transgression of Thy people was He stricken.
  }
}#}))
