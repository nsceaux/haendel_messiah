\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff << \global \clef "treble" \includeNotes "violino1" >>
        \new Staff << \global \clef "treble" \includeNotes "violino2" >>
      >>
      \new Staff <<
        \global \clef "alto" \includeNotes "viola"
      >>
    >>
    \includeNotes "vocal"
    \new Staff <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
