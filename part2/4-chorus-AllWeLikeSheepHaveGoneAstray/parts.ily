\piecePartSpecs
#`((violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (bassi #:indent 0)
   (oboe #:notes "oboe" #:indent 0)
   (reduction #:indent 0)
   (choir #:indent 0)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    All we like sheep have gone astray;
    we have turned every one to his own way;
    and the Lord hath laid on him the iniquity of us all.
  }
}#}))
