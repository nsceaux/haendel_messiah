\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:notes "vcanto")
   (reduction #:indent 10)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Surely he hath borne our griefs, and carried our sorrows.
  }
  \wordwrap {
    He was wounded for our transgressions,
    he was bruised for our iniquities:
    the chastisement of our peace was upon him,
  }
}#}))
