\new ChoirStaff \with { \haraKiriFirst } <<
  \new Staff \with { instrumentName = \markup\character Canto } \withLyrics <<
    \global \clef "treble" \includeNotes "vcanto"
  >> \includeLyrics "lyrics1"
  \new Staff \with { instrumentName = \markup\character Alto } \withLyrics <<
    \global \clef "treble" \includeNotes "valto"
  >> \includeLyrics "lyrics2"
  \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
    \global \clef "G_8" \includeNotes "vtenore"
  >> \includeLyrics "lyrics3"
  \new Staff \with { instrumentName = \markup\character Basso } \withLyrics <<
    \global \clef "bass" \includeNotes "vbasso"
  >> \includeLyrics "lyrics4"
>>
