\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi #:score-template "score")
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    The Lord gave the word: great was the company of the preachers.
  }
}#}))
