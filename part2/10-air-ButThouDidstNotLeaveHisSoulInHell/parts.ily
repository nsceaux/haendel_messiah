\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    But thou didst not leave his soul in hell;
    neither didst thou suffer Thy Holy One to see corruption.
  }
}#}))
