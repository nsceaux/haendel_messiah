\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix-figures")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Thy rebuke hath broken his heart; He is full of heaviness.
    He looked for some to have pity on Him, but there was none;
    neither found He any to comfort Him.
  }
}#}))
