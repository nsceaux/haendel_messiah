\score {
  \new StaffGroup <<
    \new GrandStaff \with {
      instrumentName = \markup\center-column { Trombe in D }
    } \transpose re do <<
      \new Staff << \global \keepWithTag#'tromba1 \includeNotes "trombe" >>
      \new Staff << \global \keepWithTag#'tromba2 \includeNotes "trombe" >>
    >>
    \new Staff \with { instrumentName = "Timpani" } <<
      \global \clef "bass" \includeNotes "tympani"
    >>
  >>
  \layout { indent = \largeindent }
}
