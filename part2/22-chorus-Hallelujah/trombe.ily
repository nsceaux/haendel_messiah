\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { s1*13 |
    s2 s4 s8 mi''16 mi'' |
    fad''8 mi'' s mi''16 mi'' fad''8 mi'' s la''16 la'' |
    la''8 la''16 la'' la''8 mi''16 mi'' fad''8 mi'' s4 |
    s1*2 |
    s2 s4 s8 la''16 la'' |
    si''8 la'' s la''16 la'' si''8 la'' s re''16 re'' |
    sol''8 fad''16 fad'' fad''8 re''16 re'' re''8 la'16 la' la'8 la'16 la' |
    fad'4 s s2 |
    s1*2 |
    s8 mi''16 mi'' mi''8 fad''16 mi'' re''8 sol''16 fad'' mi''8 la''16 sol'' |
    fad''4 s8 fad'' sold''4 la'' ~|
    la''8 la'' sold''4 la'' s8 la''16 la'' |
    la''8 fad'' s fad''16 fad'' mi''8 mi'' s4 |
    re''2 mi''4 fad'' |
    sol'' sol''2 fad''4 |
    mi''2 re'' ~|
    re'' s8 fad''16 fad'' fad''8 fad'' |
    sol'' re''16 re'' re''8 re'' la'4 s |
    s1*3 |
    s2 s4 la'' |
    la'' sol'' fad'' mi''8. re''16 |
    re''4. mi''8 fad''4 sold'' |
    la''4. la''8 re''4 sol'' |
    fad''2 s |
    s1*10 |
    s4 s8 mi'' fad'' mi'' s mi'' |
    fad'' mi'' s mi''16 mi'' fad''8 mi'' s mi''16 mi'' |
    fad''8 mi'' s4 s2 |
    s4 la'' la''2 ~|
    la''1 ~|
    la''4 ~ la''16 sol'' fad'' mi'' re''4 re'' |
    re''1 ~|
    re'' ~|
    re''4 re'' mi'' mi'' |
    mi''1 ~|
    mi'' ~|
    mi''2 fad''4 fad'' |
    fad''1 ~|
    fad'' ~|
    fad''4. fad''8 sol''4 sol'' |
    sol''1 ~|
    sol''4. sol''8 fad'' mi'' fad'' sol'' |
    mi''2 s |
    s1*4 |
    s2 re'4 re' |
    re'1 ~|
    re'4. re'8 re'4 re' |
    re'1 ~|
    re'8 re'16 re' fad'8 la' re''4 la'' |
    re''' fad'' si'' re'' |
    sol'' fad''8 mi'' mi''4. re''8 |
    re''4 s fad'' fad''8. sol''16 |
    la''4 s8 la'' fad''4 fad''8. sol''16 |
    la''4 s fad'' fad''8. sol''16 |
    la''4 s8 la'' fad''4 fad''8. sol''16 |
    la''4 s s la' |
    re'' fad'' si'' re'' |
    sol'' fad''8 mi'' mi''4. re''8 |
    re'' re'16 re' fad'8 la'16 la' re''4 re'' |
    re''8 fad'' la'' fad'' re''4 re'' |
    re''8 fad'' la'' fad'' re''4 re'' |
    re''8 fad''16 sol'' la''8 fad'' re'' fad''16 sol'' la''8 fad''16 fad'' |
    re''8 re'' s4 s fad'' |
    re''2. re''4 |
    re''1 | }
   { s1*13 |
     s2 s4 s8 la'16 la' |
     la'8 la' s la'16 la' la'8 la' s mi''16 mi'' |
     fad''8 mi''16 mi'' mi''8 la'16 la' la'8 la' s4 |
     s1*2 |
     s2 s4 s8 re''16 re'' |
     sol''8 fad'' s re''16 re'' sol''8 fad'' s fad'16 fad' |
     re'8 re'16 re' la'8 fad'16 fad' fad'8 fad'16 fad' fad'8 fad'16 fad' |
     re'4 s s2 |
     s1*2 |
     s8 la'16 la' la'8 la' fad' re'16 re' la'8 la'16 la' |
     la'4 s8 re'' mi''4. mi''8 |
     re'' la' re'' mi''16 re'' mi''4 s8 mi''16 mi'' |
     re''8 la' s fad'16 fad' la'8 la' s4 |
     R1*3 |
     s2 s8 la'16 la' la'8 la' |
     re'' re'16 re' re'8 re' fad'4 s |
     s1*3 |
     s2 s4 fad'' |
     mi'' re'' re'' la' |
     la'4. mi''8 la'4 re'' |
     mi''4. fad''8 re''4 mi'' |
     la'2 s |
     s1*10 |
     s4 s8 la' la' la' s la' |
     la' la' s la'16 la' la'8 la' s la'16 la' |
     la'8 la' s4 s2 |
     s4 r8 mi'' fad'' mi'' r mi'' |
     fad'' mi'' r mi''16 mi'' fad''8 mi'' r mi''16 mi'' |
     fad''8 mi'' r4 r2 |
     R1*9 |
     r2 re''4 re'' |
     mi''4. mi''8 re'' la' la' la' |
     la'2 s |
     s1*4 |
     s2 re'4 re' |
     re'1 ~|
     re'4. re'8 re'4 re' |
     re'1 ~|
     re'8 re'16 re' re'8 re' fad'4 re'' |
     fad'' re'' re'' re'' |
     mi'' re''8 la' la' la'16 la' la'8 la' |
     fad'4 s re'' re''8. mi''16 fad''4 s8 fad'' re''4 re''8. mi''16 fad''4 s re'' re''8. mi''16 fad''4 s8 fad'' re''4 re''8. mi''16 |
     fad''4 s s fad' |
     fad'8 re' la' fad' re''4 la' |
     mi'' re''8 la' la' la'16 la' la'8 la' |
     fad'4 r8 fad' re' re' r la' |
     re' re'' fad'' re'' re'4 re' |
     re'8 re'' fad'' re'' re'4 re' |
     re'8 re''16 mi'' fad''8 re'' re' re''16 mi'' fad''8 re''16 re'' |
     re'8 fad' s4 s la' |
     re'2. re'4 |
     fad'1 | }
   { R1*13 |
     r2 r4 r8 s |
     s4 r8 s8 s4 r8 s |
     s2. r4 |
     R1*2 |
     r2 r4 r8 s |
     s4 r8 s8 s4 r8 s |
     s1 |
     s4 r r2 |
     R1*2 |
     r8 s8 s2. |
     s4 r8 s8 s2 |
     s2. r8 s8 |
     s4 r8 s4. r4 |
     s1*3 |
     s2 r8 s4. |
     s2. r4 | \allowPageTurn
     R1*3 | \allowPageTurn
     r2 r4 s |
     s1*3 |
     s2 r | \allowPageTurn
     R1*10 | \allowPageTurn
     r4 r8 s4. r8 s |
     s4 r8 s4. r8 s |
     s4 r4 r2 |
     r4 s2. |
     s1*13 |
     s2 r |
     R1*4 | \allowPageTurn
     r2 s |
     s1*6 |
     s4 r s2 |
     s4 r8 s s2 |
     s4 r s2 |
     s4 r8 s s2 |
     s4 r r s |
     s1*6 |
     s4 r r s |
     s1*2 | }
 >>
 