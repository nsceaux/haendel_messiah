\piecePartSpecs
#`((violino1)
   (violino2)
   (trombe-timpani)
   (oboe #:notes "oboe")
   (viola)
   (bassi)
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Hallelujah! for the Lord God omnipotent reigneth.
  }
  \wordwrap {
    The kingdoms of this world are become the kingdoms of our Lord,
    and of His Christ:
    and He shall reign for ever and ever.
  }
  \wordwrap {
    KING OF KINGS, LORD OF LORDS.
  }
}#}))
