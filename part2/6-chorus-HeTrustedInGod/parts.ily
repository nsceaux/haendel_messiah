\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (oboe #:notes "oboe")
   (bassi)
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    He trusted in God that he would deliver him:
    let him deliver him, if he delight in him.
  }
}#}))
