\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Thou art gone up on high,
    Thou hast led captivity captive,
    and received gifts for men;
    yea, even for Thine enemies,
    that the Lord God might dwell among them.
  }
}#}))
