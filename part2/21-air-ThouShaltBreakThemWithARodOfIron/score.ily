\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = "Violini" } <<
      \global \clef "treble" \includeNotes "violini"
    >>
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
