\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Thou shalt break them with a rod of iron;
    Thou shalt dash them in pieces like a potter's vessel.
  }
}#}))
