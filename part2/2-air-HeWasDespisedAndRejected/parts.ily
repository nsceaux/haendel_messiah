\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    He is despised and rejected of men:
    a man of sorrows, and acquainted with grief.
  }
  \wordwrap {
    He gave His back to the smiters,
    and His cheeks to them that plucked off the hair:
    He hid not His face from shame and spitting.
  }
}#}))
