\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Let all the angels of God worship Him.
  }
}#}))
