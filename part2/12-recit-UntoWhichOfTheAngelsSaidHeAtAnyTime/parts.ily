\piecePartSpecs
#`((bassi #:score-template "score-voix-figures")
   (reduction #:score "score")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    For unto which of the angels said He at any time,
    Thou art my Son, this day have I begotten thee?
  }
}#}))
