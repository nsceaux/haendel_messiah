\piecePartSpecs
#`((violino1 #:indent 0)
   (violino2 #:indent 0)
   (viola #:indent 0)
   (bassi #:indent 0)
   (oboe #:notes "vcanto" #:indent 0)
   (reduction #:indent 0)
   (choir #:indent 0)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    and with His stripes we are healed.
  }
}#}))
