\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi #:score-template "score")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    How beautiful are the feet of them that preach the gospel of peace,
    and bring glad tidings of good things!
  }
}#}))
