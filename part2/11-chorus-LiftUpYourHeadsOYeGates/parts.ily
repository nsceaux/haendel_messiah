\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:score "score-oboe")
   (reduction)
   (choir #:score "score-choir")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Lift up your heads, O ye gates;
    and be ye lift up, ye everlasting doors;
    and the King of Glory shall come in.
  }
  \wordwrap {
    Who is the King of Glory?
    The Lord strong and mighty, the Lord mighty in battle.
  }
  \wordwrap {
    Lift up your heads, O ye gates;
    and be ye lift up, ye everlasting doors;
    and the King of Glory shall come in.
  }
  \wordwrap {
    Who is the King of Glory?
    The Lord of Hosts, He is the King of Glory.
  }
}#}))
