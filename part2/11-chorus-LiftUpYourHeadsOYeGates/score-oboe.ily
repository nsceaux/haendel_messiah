\score {
  \new GrandStaff <<
    \new Staff << \global \clef "treble" \includeNotes "vcanto1" >>
    \new Staff \with { \haraKiri } <<
      \global \clef "treble" \includeNotes "vcanto2"
    >>
  >>
  \layout { }
}
