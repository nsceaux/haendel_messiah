\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\character Canto I } \withLyrics <<
      { s1*6\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s2 s4. s8^"Canto I II"}
      \global \clef "treble" \includeNotes "vcanto1"
    >> \includeLyrics "lyrics11"
    \new Staff \with {
      instrumentName = \markup\character Canto II
      \haraKiri
   } \withLyrics <<
     \characterName \markup "Canto II"
     \global \clef "treble" \includeNotes "vcanto2"
   >> \includeLyrics "lyrics12"
   \new Staff \with { instrumentName = \markup\character Alto } \withLyrics <<
     \global \clef "treble" \includeNotes "valto"
   >> \includeLyrics "lyrics2"
   \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
     \global \clef "G_8" \includeNotes "vtenore"
   >> \includeLyrics "lyrics3"
   \new Staff \with { instrumentName = \markup\character Basso } \withLyrics <<
     \global \clef "bass" \includeNotes "vbasso"
   >> \includeLyrics "lyrics4"
 >>
  \layout {
    indent = #(or (*score-indent*) largeindent)
    ragged-last = #(*score-ragged*)
  }
}
