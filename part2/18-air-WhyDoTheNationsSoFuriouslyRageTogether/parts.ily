\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Why do the nations so furiously rage together,
    why do the people imagine a vain thing?
  }
  \wordwrap {
    The kings of the earth rise up,
    and the rulers take counsel together,
    against the Lord, and against His Anointed,
  }
}#}))
