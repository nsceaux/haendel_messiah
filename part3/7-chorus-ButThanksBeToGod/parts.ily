\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    But thanks be to God,
    who giveth us the victory through our Lord Jesus Christ.
  }
}#}))
