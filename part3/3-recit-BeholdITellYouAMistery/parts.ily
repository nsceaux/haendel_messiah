\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix")
   (reduction #:system-count 2)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Behold, I tell you a mystery: We shall not all sleep;
    but we shall all be changed.
  }
  \wordwrap {
    In a moment, in a twinkling of an eye, at the last trumpet;
  }
}#}))
