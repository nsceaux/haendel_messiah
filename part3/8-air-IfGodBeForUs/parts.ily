\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    If God be for us, who can be against us?
  }
  \wordwrap {
    Who shall lay any thing to the charge of God's elect?
    It is God that justifieth.
  }
  \wordwrap {
    Who is he that condemneth?
    It is Christ that died, yea rather,
    that is risen again, who is at the right hand of God,
    who makes intercession "for us."
  }
}#}))
