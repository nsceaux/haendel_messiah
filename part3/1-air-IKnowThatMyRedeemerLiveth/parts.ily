\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    I know that my redeemer liveth,
    and that He shall stand on the latter day upon the earth:
  }
  \wordwrap {
    And though worms destroy this body,
    yet in my flesh shall I see God.
  }
  \wordwrap {
    For now is Christ risen from the dead,
    the firstfruits of them that sleep.
  }
}#}))
