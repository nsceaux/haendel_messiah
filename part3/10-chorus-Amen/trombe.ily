\twoVoices #'(tromba1 tromba2 trombe) <<
  { s1*30 |
    fad''4-! s8 sol'' la''4 la'' |
    la'' s2 fad''4 |
    re'' s2 la''4 ~|
    la'' sol''2 fad''4 |
    mi''2 re''4 s |
    s1*2 |
    mi''4 s8 mi'' la''4 sold'' |
    la'' s2. |
    s1 |
    s2. la''4 |
    si'' mi'' mi'' s |
    s1*2 |
    la''4 mi'' fad'' s |
    s1*29 |
    re''4. mi''8 fad'' sol'' la'' mi'' |
    fad''2 mi''4 la'' |
    sol''2 fad'' |
    mi''4 mi'' la' la'' |
    sol'' fad''8 sol'' la''4. sol''8 |
    fad''4 la'' ~ la''8 sol'' fad'' mi'' |
    re''4 la'' ~ la''8 sol'' fad'' mi'' |
    re''4 mi''8 fad'' sol''4 la'' |
    si''4. la''8 sol'' fad'' mi'' re'' |
    mi''2 mi'' |
    s1 |
    re''2 fad''4. re''8 |
    mi''1 |
    re''\breve*1/2 | }
  { s1*30 |
    re''4-! s8 mi'' fad''4 mi'' |
    fad'' s2 fad'4 |
    fad' s2 la'4 |
    fad' re' la' la'' |
    sol''2 fad''4 s |
    s1*2 |
    la'4 s8 la' mi''4 mi'' |
    mi'' s2. |
    s1 |
    s2. mi''4 |
    re''4. re'8 la'4 s |
    s1*2 |
    mi''4 la' re' s |
    s1*29 |
    fad'4 re' la' r8 la' |
    la'4 re' la' la' |
    mi''2 re'' |
    la'4 la' fad' fad'' |
    mi'' re'' mi''2 |
    re''4 r la'2 ~|
    la'8 la' fad' fad' re'4 la' |
    re'4. re'8 re''4 re'' |
    re'' mi'' mi'' r8 la' |
    la'2 la' |
    s1 |
    la'2 re''4 re'8 fad' |
    la'2. la'4 |
    fad'\breve*1/2 | }
  { R1*30 |
    s4 r8 s s2 |
    s4 r r s |
    s r r s |
    s1 |
    s2. r4 |
    R1*2 |
    s4 r8 s s2 |
    s4 r4 r2 |
    R1 |
    r2 r4 s4 |
    s2. r4 |
    R1*2 |
    s2. r4 |
    R1*29 |
    s1*10 |
    R1^\fermata |
    s1*3 | }
>>
