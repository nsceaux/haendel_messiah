\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'trombe \includeNotes "trombe" >>
      \new Staff << \global \clef "bass" \includeNotes "tympani" >>
    >>
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff << \global \clef "treble" \includeNotes "violino1" >>
        \new Staff << \global \clef "treble" \includeNotes "violino2" >>
      >>
      \new Staff << \global \clef "alto" \includeNotes "viola" >>
    >>
    \includeNotes "vocal"
    \new Staff \with {
      instrumentName = "B."
      shortInstrumentName = "B."
    } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
      %\modVersion { s1*20\break s1*10\break }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
