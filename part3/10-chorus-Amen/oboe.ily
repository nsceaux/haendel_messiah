\clef "treble" R1*15 |
la'4. si'8 dod'' re'' mi'' si' | dod'' 
la' fad''2 mi''4 |
la' re''2 dod''4 |
fad''2 mi'' |
re'' dod'' |
R1*10 |
re''4. dod''8 re''4 la' |
re''8 la' re''4-! dod''-! fad''-! |
r mi'' la' la'' |
re'' sol''2 fad''4 |
mi''2 re'' |
R1*2 |
mi''4. re''8 dod''4 si' |
la' r r sold' |
fad' si' r mi'' ~ |
mi'' re''2 dod''4 |
si'2 la'4. si'8 |
dod'' re'' mi''4 ~ mi''8 re''16 dod'' si'8 dod'' |
re'' mi'' fad''4 ~ fad''8 mi''16 re'' dod''8 re'' |
mi''4 la' re'4. mi'8 |
fad' sol' la'4 ~ la'8 sol'16 fad' mi'8 fad' |
sol' la' si'4 ~ si'8 la'16 sol' fad'8 sol' |
la' si'16 dod'' re''8 dod'' si' dod''16 re'' mi''8 re'' |
dod'' si' la'4 r2 |
R1 |
re''4. mi''8 fad'' sol'' la'' mi'' |
fad''4 fad' r2 |
r r4 fad'' |
si' mi''2 re''4 ~|
re'' dod'' si'2 |
la' r |
R1 |
r2 fad'4. sold'8 |
lad' si' dod''2 si'4 ~|
si' la' sold'2 |
fad' r |
R1 |
r2 re''4. dod''8 |
si'4 mi'' ~ mi''8 re'' dod'' si' |
la' si'16 dod'' re''4 ~ re''8 do'' si' la' |
sol' la'16 si' do''2 do''4 |
do'' ( si' ) la'2 |
sol'4. la'8 si' dod'' re''4 ~|
re''8 dod''16 si' la'8 si' dod'' re'' mi''4 ~|
mi''8 re''16 dod'' si'8 dod'' re'' mi'' fad'' sol'' |
la''4 mi'' re''2 |
dod''4 mi'' ~ mi''8 re'' dod''4 |
fad''4. mi''8 re'' dod'' si' dod''16 re'' |
mi''4. re''8 dod'' si' la' si'16 dod'' |
re''4. mi''8 fad'' sol'' la'' mi'' |
fad''2 mi''4 r |
R1 |
r4 mi'' la' re'' |
dod'' re''2 dod''4 |
re'' la'' ~ la''8 sol'' fad'' mi'' |
re''4 re'8 mi' fad' sol' la'4 |
sol'4. la'8 si' dod'' re'' la' |
si'4 sol'' ~ sol''8 fad'' mi'' re'' |
dod''2 dod'' |
R1^\fermata |
re''2 re'' |
re'' ( dod'' )|
re''\breve*1/2 |
