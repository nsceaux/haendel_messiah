\piecePartSpecs
#`((trombe-timpani)
   (violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Worthy is the Lamb that was slain to receive power,
    and riches, and wisdom, and strength, and honour,
    and glory, and blessing.
  }
  \wordwrap {
    Blessing, and honour, glory and power,
    be unto Him that sitteth upon the throne,
    and unto the Lamb for ever and ever.
  }
}#}))
