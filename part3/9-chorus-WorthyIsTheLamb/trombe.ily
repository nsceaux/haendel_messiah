\clef "treble"
\twoVoices #'(tromba1 tromba2 trombe) <<
  { s4 fad''2 la''4 |
    re'''4. re'''8 la''2 |
    sol''4 fad'' mi''2 |
    s4 mi'' mi''4. mi''8 |
    re''4. re''8 re''4. mi''8 |
    fad''4 fad''2 mi''4 |
    fad''2 s8 fad'' fad'' fad'' |
    re'' re'' s re'' mi'' mi'' s mi'' |
    fad'' fad'' s fad'' sol''4 s8 sol'' |
    fad'' fad'' s la'' sold'' sold'' s si'' |
    la''4 mi'' s2 |
    s1*7 |
    s2 s8 la'' la'' la'' |
    sol'' sol'' s sol'' fad'' fad'' s fad'' |
    re'' re'' s sol'' mi''4 s8 la'' |
    la'' sol'' s sol'' sol'' sol'' s sol'' |
    fad''4 fad'' s2 |
    s1*29 |
    s1*2 |
    s2. fad'8 fad'16 fad' |
    la'8 fad' s4 re''8 la' s4 |
    fad''8 re'' s fad'' la'' fad''16 fad'' re''8 sol'' |
    fad''4 r8 la'' si''16 la'' sol'' fad'' mi''8 la'' |
    re'' mi''16 fad'' sol''8 mi'' fad''8. mi''16 re''8 sol''16 fad'' |
    mi''4 re'' ~ re''8 mi''16 fad'' sol''8 sol'' |
    sol'' fad''16 mi'' fad''4 s2 |
    s1 |
    s2 s4 s8 la'' |
    la''16 sol'' fad'' mi'' re''8 re'' mi'' mi'' s mi'' |
    fad'' fad'' s fad'' sol'' sol'' s mi'' |
    la'' la'' r la'' si'' si'' r la'' |
    la'' mi''16 mi'' mi''8 la'' la''4 sol''8 mi'' |
    fad''4 mi''8. re''16 re''4 s8 mi'' |
    fad''2 la''4 la'' |
    la''2 sold'' |
    la''1 | }
  { s4 re''2 mi''4 |
    fad''4. fad''8 re''2 |
    mi''4 la' la'2 |
    s4 mi'' fad'4. fad'8 |
    fad'4. fad'8 fad'4 fad' |
    fad' fad' re' mi'' |
    fad'2 s8 fad' fad' fad' |
    fad' fad' s fad' la' la' s la' |
    la' la' s re'' mi''4 s8 mi'' |
    re'' re'' s re'' mi'' mi'' s mi'' |
    mi''4 la' s2 |
    s1*7 |
    s2 s8 fad'' fad'' fad'' |
    mi'' mi'' s mi'' re'' re'' s re'' |
    re' re' s re' la'4 s8 mi'' |
    re'' re'' s re'' mi'' mi'' s mi'' |
    re''4 re'' s2 |
    s1*29 |
    s1*2 |
    s2. re'8 re'16 re' |
    fad'8 re' s4 la'8 fad' s4 |
    re''8 la' s re'' fad'' re''16 re'' re'8 re'' |
    re''4 r r r8 la' |
    fad'4 r8 la' la' fad' r re' |
    la'4 r8 fad' re' re' re'' re'' |
    la' re' re''4 s2 |
    s1 |
    s2 s4 s8 la' |
    re'' re'' r4 r s8 la' |
    la' la' s re'' re'' mi'' s la' |
    mi'' mi'' fad'' fad'' re'' re'' mi'' mi'' |
    mi'' la'16 la' la'8 mi'' fad''4 re''8 mi'' |
    re''4 la'8. la'16 fad'4 s8 la' |
    la'2 mi''4 mi'' |
    re''1 |
    mi'' | }
  { r4 s2. |
    s1*2 |
    r4 s2. |
    s1*2 |
    s2 r8 s4. |
    s4 r8 s4. r8 s |
    s4 r8 s4. r8 s |
    s4 r8 s4. r8 s |
    s2 r2 |
    R1*7 |
    r2 r8 s4. |
    s4 r8 s4. r8 s |
    s4 r8 s4. r8 s |
    s4 r8 s4. r8 s |
    s2 r2 | \allowPageTurn
    R1*29 | \allowPageTurn
    r2 r4 la8 la16 la |
    re'8 re' re' re' re' re' re' re' |
    re' la16 la la8 la re' re'16 re' s4 |
    s r4 s r4 |
    s r8 s s2 |
    s1*3 |
    s2 r2 |
    R1 |
    r2 r4 r8 s |
    s2. r8 s |
    s4 r8 s4. r8 s |
    s1*2 |
    s2. r8 s |
    s1*3 | }
>>
