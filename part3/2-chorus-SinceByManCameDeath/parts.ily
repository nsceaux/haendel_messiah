\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (oboe #:notes "oboe")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Since by man came death,
    by man came also the resurrection of the dead.
  }
  \wordwrap {
    For as in Adam all die, even so in Christ shall all be made alive.
  }
}#}))
