\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff << \global \clef "treble" \includeNotes "violino1" >>
        \new Staff << \global \clef "treble" \includeNotes "violino2" >>
      >>
      \new Staff \with { instrumentName = "Viola"} <<
        \global \clef "alto" \includeNotes "viola"
      >>
    >>
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
      \modVersion { s1*6 \break s1*10 \break s1*6 \break }
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
