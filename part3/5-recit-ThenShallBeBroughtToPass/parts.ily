\piecePartSpecs
#`((bassi #:score-template "score-voix-figures")
   (reduction #:score "score")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Then shall be brought to pass the saying that is written,
    ‘Death is swallowed up in victory.’
  }
}#}))
