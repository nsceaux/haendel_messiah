\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\character Contr’alto } \withLyrics <<
      \global \clef "treble" \includeNotes "valto"
    >> \includeLyrics "lyrics1"
    \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
      \global \clef "G_8" \includeNotes "vtenore"
    >> \includeLyrics "lyrics2"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
