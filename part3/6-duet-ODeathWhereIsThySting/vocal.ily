\new ChoirStaff <<
  \new Staff \with { instrumentName = \markup\character Contr’alto } \withLyrics <<
    \global \clef "treble" \includeNotes "valto"
  >> \includeLyrics "lyrics1"
  \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
    \global \clef "G_8" \includeNotes "vtenore"
  >> \includeLyrics "lyrics2"
>>
