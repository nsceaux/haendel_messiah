\piecePartSpecs
#`((bassi)
   (reduction #:score "score")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    O death, where is thy sting?
    O grave where is thy victory?
  }
  \wordwrap {
    The sting of death is sin; and the strength of sin is the law.
  }
}#}))
