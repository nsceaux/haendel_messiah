\score {
  \new StaffGroupNoBar <<
    \new Staff \with { \haraKiri instrumentName = "Tromba" } <<
      { \noHaraKiri s4 s2.*155 \revertNoHaraKiri s2 \break }
      \global \clef "treble" \includeNotes "tromba"
    >>
    \new StaffGroupNoBracket \with { \haraKiri } <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff <<
          { \noHaraKiri s4 s2.*155 \revertNoHaraKiri }
          \global \clef "treble" \includeNotes "violino1"
        >>
        \new Staff <<
          { \noHaraKiri s4 s2.*155 \revertNoHaraKiri }
          \global \clef "treble" \includeNotes "violino2"
        >>
      >>
      \new Staff \with { \haraKiri instrumentName = "Viola" } <<
        { \noHaraKiri s4 s2.*155 \revertNoHaraKiri }
        \global \clef "alto" \includeNotes "viola"
      >>
    >>
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}
