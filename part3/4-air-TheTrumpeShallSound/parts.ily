\piecePartSpecs
#`((trombe-timpani)
   (violino1)
   (violino2)
   (viola)
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    The trumpet shall sound,
    and the dead shall be raised incorruptible,
    and we shall be changed.
  }
  \wordwrap {
    For this corruptible must put on incorruption,
    and this mortal must put on immortality.
  }
}#}))
