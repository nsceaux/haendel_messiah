\score {
  \new Staff \with {
    instrumentName = \markup\center-column { Tromba in D }
  } \transpose re do <<
    \global \clef "treble" \includeNotes "tromba"
  >>
  \layout { indent = \largeindent }
}
