\paper {
  %% de la place sous la basse pour pouvoir noter les chiffrages
  score-markup-spacing.padding = #4
  system-system-spacing.padding = #4
  last-bottom-spacing.padding = #4
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = \markup\smallCaps Messiah }
  %% Title page
  \markup\null\pageBreak
  %% Table of contents
  \markuplist\abs-fontsize-lines#8
  %\with-line-width-ratio#0.7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
  \markup\center-column {
    \vspace#2
    \sep
    \vspace#2
    \line { Based upon the Deutsche Händelgesellschaft Edition }
    \line { Edited by Frideric Chrysander }
  }
}
