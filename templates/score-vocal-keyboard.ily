\score {
  <<
    \includeNotes "vocal"
    \includeNotes "keyboard"
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(or (*score-indent*) largeindent)
    ragged-last = #(*score-ragged*)
  }
}
