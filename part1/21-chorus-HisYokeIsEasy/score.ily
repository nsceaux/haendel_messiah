\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff \with { instrumentName = "Violini" } <<
        \new Staff << \global \clef "treble" \includeNotes "violino1" >>
        \new Staff << \global \clef "treble" \includeNotes "violino2" >>
      >>
      \new Staff \with { instrumentName = "Viola" } <<
        \global \clef "alto" \includeNotes "viola"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with { instrumentName = \markup\character Canto } \withLyrics <<
        { s4. s8^"Tutti c. Oboe I & II unis." }
        \global \clef "treble" \includeNotes "vcanto"
      >> \includeLyrics "lyrics1"
      \new Staff \with { instrumentName = \markup\character Alto } \withLyrics <<
        \global \clef "treble" \includeNotes "valto"
      >> \includeLyrics "lyrics2"
      \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
        \global \clef "G_8" \includeNotes "vtenore"
      >> \includeLyrics "lyrics3"
      \new Staff \with { instrumentName = \markup\character Basso } \withLyrics <<
        \global \clef "bass" \includeNotes "vbasso"
      >> \includeLyrics "lyrics4"
    >>
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}