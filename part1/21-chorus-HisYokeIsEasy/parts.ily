\piecePartSpecs
#`((violino1)
   (violino2)
   (oboe #:notes "oboe")
   (viola)
   (bassi)
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    His yoke is easy, and his burden is light.
    }
}#}))
