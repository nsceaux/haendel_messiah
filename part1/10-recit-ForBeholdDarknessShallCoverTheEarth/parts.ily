\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (reduction)
   (bassi #:score-template "score-voix-figures")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    For, behold, darkness shall cover the earth,
    and gross darkness the people;
    but the Lord shall arise upon thee,
    and His glory shall be seen upon thee.
  }
  \wordwrap {
    And the Gentiles shall come to thy light,
    and kings to the brightness of thy rising.
  }
}#}))

