\new Staff \with { instrumentName = \markup\character Contr’alto } \withLyrics <<
  \global \keepWithTag #'voix \includeNotes "voix"
>> \keepWithTag #'voix \includeLyrics "lyrics"
