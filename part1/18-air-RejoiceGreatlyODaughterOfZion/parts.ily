\piecePartSpecs
#`((violino1 #:notes "violini" #:tag-notes violino1)
   (violino2 #:notes "violini" #:tag-notes violino2)
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Rejoice greatly, O daughter of Zion;
    Shout, O daughter of Jerusalem:
    behold, thy King cometh unto thee.
    He is the righteous Savior.
  }
  \wordwrap {
    And he shall speak peace unto the heathen.
  }
}#}))
