\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Violino II" } <<
      \global \clef "treble" \includeNotes "violino2"
    >>
    \new Staff \with { instrumentName = "Violino III" } <<
      \global \clef "treble" \transpose do' do { \includeNotes "violino1" }
    >>
  >>
  \layout { indent = \largeindent }
}