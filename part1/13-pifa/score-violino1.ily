\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "Violino I" } <<
      \global \clef "treble" \includeNotes "violino1"
    >>
    \new Staff \with { instrumentName = "Violino III" } <<
      \global \clef "treble" \transpose do' do { \includeNotes "violino1" }
    >>
  >>
  \layout { indent = \largeindent }
}