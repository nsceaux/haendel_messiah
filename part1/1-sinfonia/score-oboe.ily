\score {
  \new GrandStaff \with { instrumentName = "Violini" } <<
    \new Staff << \global \clef "treble" \includeNotes "violino1" >>
    \new Staff <<
      \global \clef "treble" \includeNotes "violino2"
      { s1*13 \break }
    >>
  >>
  \layout { indent = \largeindent }
}
