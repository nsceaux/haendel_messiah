\score {
  \new StaffGroup <<
    \new GrandStaff \with { instrumentName = "Violini" } <<
      \new Staff << \global \clef "treble" \includeNotes "violino1" >>
      \new Staff << \global \clef "treble" \includeNotes "violino2" >>
    >>
    \new Staff \with { instrumentName = "Viola" } <<
      \global \clef "alto" \includeNotes "viola"
    >>
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
      \modVersion { s1*13 \break }
      \includeFigures "figures"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}