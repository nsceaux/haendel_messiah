\piecePartSpecs
#(let ((breaks #{ s1*13 \break #}))
       `((violino1 #:music ,breaks)
         (violino2 #:music ,breaks)
         (oboe #:score "score-oboe")
         (viola #:music ,breaks)
         (bassi #:music ,breaks)
         (reduction #:score-template "score-keyboard")))
