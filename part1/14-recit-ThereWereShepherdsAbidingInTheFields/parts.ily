\piecePartSpecs
#`((reduction #:score "score")
   (bassi #:score-template "score-voix")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    There were shepherds abiding in the field,
    keeping watch over their flocks by night.
  }
}#}))
