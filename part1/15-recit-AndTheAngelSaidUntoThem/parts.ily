\piecePartSpecs
#`((bassi #:score-template "score-voix-figures")
   (reduction #:score "score")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    And the angel said unto them, Fear not;
    for, behold, I bring you good tidings of great joy,
    which shall be to all people.
  }
  \wordwrap {
    For unto you is born this day in the city of David a Saviour,
    which is Christ the Lord.
  }
}#}))
