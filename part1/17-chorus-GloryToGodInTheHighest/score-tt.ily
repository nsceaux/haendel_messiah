\score {
  \new GrandStaff \with {
    instrumentName = \markup\center-column { Trombe "in D" }
  } \transpose re do <<
    \new Staff << \global \clef "treble" \includeNotes "tromba1" >>
    \new Staff << \global \clef "treble" \includeNotes "tromba2" >>
  >>
 \layout { indent = \largeindent }
}