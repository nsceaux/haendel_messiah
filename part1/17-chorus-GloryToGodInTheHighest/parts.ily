\piecePartSpecs
#`((violino1)
   (violino2)
   (trombe-timpani)
   (oboe #:notes "oboe")
   (viola)
   (bassi #:clef "tenor")
   (reduction)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Glory to God in the highest, and on earth peace,
    good will toward men.
  }
}#}))
