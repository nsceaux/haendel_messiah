\piecePartSpecs
#`((violino1 #:score-template "score-voix"
             #:indent 0)
   (violino2 #:score-template "score-voix"
             #:indent 0)
   (viola #:score-template "score-voix"
          #:indent 0)
   (bassi #:score-template "score-voix-figures"
          #:indent 0)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    And lo! the angel of the Lord came upon them,
    and the glory of the Lord shone round about them:
    and they were sore afraid.
  }
}#}))
