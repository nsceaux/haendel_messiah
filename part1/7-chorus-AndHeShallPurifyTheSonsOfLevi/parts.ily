\piecePartSpecs
#`((violino1)
   (violino2)
   (oboe #:notes "oboe")
   (viola)
   (bassi)
   (choir)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    And He shall purify the sons of Levi,
    that they may offer unto the Lord an offering in righteousness.
  }
}#}))
