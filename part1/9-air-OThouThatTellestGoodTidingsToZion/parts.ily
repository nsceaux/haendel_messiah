\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (reduction)
   (bassi)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    O thou that tellest good tidings to Zion,
    get thee up into the high mountain;
    O thou that tellest good tidings to Jerusalem,
    lift up thy voice with strength;
    lift it up, and be not afraid;
    say unto the cities of Judah, Behold your God!
  }
  \wordwrap {
    Arise, shine; for thy light is come,
    and the glory of the Lord is risen upon thee.
  }
}#}))
