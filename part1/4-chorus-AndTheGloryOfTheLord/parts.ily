\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (vocal)
   (oboe #:notes "oboe")
   (bassi)
   (choir)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    And the glory of the Lord shall be revealed,
    and all flesh shall see it together:
    for the mouth of the Lord hath spoken it.
  }
}#}))
