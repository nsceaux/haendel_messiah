\piecePartSpecs
#`((violino1)
   (violino2)
   (oboe #:score "score-oboe")
   (viola)
   (bassi)
   (reduction #:indent 10)
   (choir)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    For unto us a Child is born, unto us a Son is given:
    and the government shall be upon His shoulder:
    and His name shall be called Wonderful, Counsellor,
    the mighty God, the everlasting Father, the Prince of Peace.
  }
}#}))
