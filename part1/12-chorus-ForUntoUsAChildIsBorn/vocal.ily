\new ChoirStaff \with { \haraKiriFirst } <<
  \new Staff \with { instrumentName = \markup\character Canto } \withLyrics <<
    \global \clef "treble" \includeNotes "vcanto"
    { s1*6 s4 \noHaraKiri s2. s1*83 \revertNoHaraKiri }
  >> \includeLyrics "lyrics1"
  \new Staff \with { instrumentName = \markup\character Alto } \withLyrics <<
    \global \clef "treble" \includeNotes "valto"
    { s1*6 s4 \noHaraKiri s2. s1*83 \revertNoHaraKiri }
  >> \includeLyrics "lyrics2"
  \new Staff \with { instrumentName = \markup\character Tenore } \withLyrics <<
    \global \clef "G_8" \includeNotes "vtenore"
    { s1*6 s4 \noHaraKiri s2. s1*83 \revertNoHaraKiri }
  >> \includeLyrics "lyrics3"
  \new Staff \with { instrumentName = \markup\character Basso } \withLyrics <<
    \global \clef "bass" \includeNotes "vbasso"
    { s1*6 s4 \noHaraKiri s2. s1*83 \revertNoHaraKiri }
  >> \includeLyrics "lyrics4"
>>
