\score {
  \new GrandStaff <<
    \new Staff << \global \clef "treble" \includeNotes "oboe1" >>
    \new Staff << \global \clef "treble" \includeNotes "oboe2" >>
  >>
  \layout { }
}
