\score {
  \new ChoirStaff <<
    \new Staff \with { instrumentName = \markup\center-column { Violini Viola } } <<
      \global \clef "treble" \includeNotes "violini"
    >>
    \includeNotes "vocal"
    \new Staff \with { instrumentName = "Bassi" } <<
      \global \clef "bass" \includeNotes "bassi"
    >>
  >>
  \layout { indent = \largeindent }
  \midi { }
}