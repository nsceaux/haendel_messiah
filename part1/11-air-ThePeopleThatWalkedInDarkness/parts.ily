\piecePartSpecs
#`((violino1 #:notes "violini")
   (violino2 #:notes "violini")
   (viola #:notes "violini")
   (bassi #:score-template "score")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    The people that walked in darkness have seen a great light:
    and they that dwell in the land of the shadow of death,
    upon them hath the light shined.
  }
}#}))
