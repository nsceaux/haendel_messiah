\piecePartSpecs
#`((bassi #:score-template "score-voix-figures")
   (reduction #:score "score")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Then the eyes of the blind shall be opened,
    and the ears of the deaf shall be unstopped.
  }
  \wordwrap {
    Then shall the lame man leap as an hart,
    and the tongue of the dumb shall sing.
  }
}#}))
