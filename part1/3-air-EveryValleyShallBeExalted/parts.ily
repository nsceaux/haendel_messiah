\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi #:score-template "score")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Every valley shall be exalted, and every mountain and hill made low,
    the crooked straight, and the rough places plain.
  }
}#}))
