\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix-figures")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Thus saith the Lord of Hosts; Yet once, a little while and I will shake the heavens, and the earth, and the sea, and the dry land;
  }
  \wordwrap {
    And I will shake all nations, and the desire of all nations shall come.
  }
  \wordwrap {
    The Lord, whom ye seek, shall suddenly come to His temple,
    even the messenger of the covenant, whom ye delight in:
    Behold, He shall come, saith the Lord of Hosts.
  }
}#}))
