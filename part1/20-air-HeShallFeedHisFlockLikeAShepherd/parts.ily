\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (bassi)
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    He shall feed his flock like a shepherd;
    and he shall gather the lambs with His arm,
    and carry them in His bosom,
    and shall gently lead those that are with young.
  }
  \wordwrap {
    Come unto Him, all ye that labour and are heavy laden,
    and He will give you rest.
  }
  \wordwrap {
    Take his yoke upon you, and learn of Him;
    for he is meek and lowly of heart:
    and ye shall find rest unto your souls.
  }
}#}))
