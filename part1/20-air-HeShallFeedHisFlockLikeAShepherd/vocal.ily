\new Staff \with {
  instrumentName = \markup\center-column\smallCaps { Alto Soprano }
} \withLyrics <<
  \global \includeNotes "voix"
>> \includeLyrics "lyrics"
