\piecePartSpecs
#`((violino1 #:score-template "score-voix")
   (violino2 #:score-template "score-voix")
   (viola #:score-template "score-voix")
   (bassi #:score-template "score-voix-figures")
   (reduction)
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Comfort ye, comfort ye my people, saith your God.
  }
  \wordwrap {
    Speak ye comfortably to Jerusalem, and cry unto her,
    that her warfare is accomplished, that her iniquity is pardoned.
  }
  \wordwrap {
    The voice of him that crieth in the wilderness,
    Prepare ye the way of the Lord, make straight in the desert a
    highway for our God.
  }
}#}))
