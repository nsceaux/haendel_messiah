\piecePartSpecs
#`((reduction #:score "score")
   (bassi #:score-template "score-voix-figures")
   (silence #:on-the-fly-markup , #{\markup\text-column {
  \wordwrap {
    Behold, a virgin shall conceive, and bear a Son,
    and shall call his name EMMANUEL, God with us.
  }
}#}))
