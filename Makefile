OUTPUT_DIR=out
DELIVERY_DIR=delivery
NENUVAR_LIB_PATH:=$(shell pwd)/../../nenuvar-lib
LILYPOND_OPTIONS=--loglevel=WARN -ddelete-intermediate-files -dno-protected-scheme-parsing
LILYPOND_CMD=lilypond -I$(shell pwd) -I$(NENUVAR_LIB_PATH) $(LILYPOND_OPTIONS)
PROJECT=Haendel_Messiah
PARTS_auto=oboe violino1 violino2 viola bassi
PARTS=$(PARTS_auto) reduction choir trombe-timpani
CONDUCTEUR=$(PROJECT)_(lead-sheet)
###
define PART_template
 $(1):
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_($(1))' -dpart=$(1) part.ly
 .PHONY: $(1)
endef

$(foreach part,$(PARTS_auto),$(eval $(call PART_template,$(part))))

reduction:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_(reduction)' \
	-dpart=reduction reduction.ly
.PHONY: keyboard

choir:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_(choir)' \
	-dpart=choir choir.ly
.PHONY: choir

trombe-timpani:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(PROJECT)_(trombe-timpani)' \
	-dpart=trombe-timpani part-tt.ly
.PHONY: trombe-timpani

define DELIVERY_PART_template
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT)_($(1)).pdf' ] ; then mv -fv '$(OUTPUT_DIR)/$(PROJECT)_($(1)).pdf' $(DELIVERY_DIR)/; fi;

endef

conducteur:
	$(LILYPOND_CMD) -o '$(OUTPUT_DIR)/$(CONDUCTEUR)' main.ly

parts: $(PARTS)

delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e '$(OUTPUT_DIR)/$(CONDUCTEUR).pdf' ]; then mv -fv '$(OUTPUT_DIR)/$(CONDUCTEUR).pdf' $(DELIVERY_DIR)/; fi
	$(foreach part,$(PARTS),$(call DELIVERY_PART_template,$(part)))
	@if [ -e "$(OUTPUT_DIR)/$(CONDUCTEUR)-1.midi" ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT)*.midi; fi
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT)_(livret).html' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT)_(livret).html' $(DELIVERY_DIR)/; fi
	@if [ -e '$(OUTPUT_DIR)/$(PROJECT)_(livret).txt' ]; then mv -fv '$(OUTPUT_DIR)/$(PROJECT)_(livret).txt' $(DELIVERY_DIR)/; fi
clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)_* $(OUTPUT_DIR)/$(PROJECT).*
all: check parts conducteur delivery clean
check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in grand-parent directory:"; \
	  echo " cd ../.. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: parts conducteur clean all check delivery
