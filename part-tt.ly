\version "2.23.4"
\include "common.ily"

\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = \markup\smallCaps Messiah }
  %% Title page
  \markup\null\pageBreak
}

\part "Part I"
%% n°17
\pieceTocNb "1-17" \markup\wordwrap {
  Chorus: \italic { Glory to God in the highest }
}
\includeScore "part1/17-chorus-GloryToGodInTheHighest"
\newBookPart #'()

\part "Part II"
%% n°22
\pieceTocNb "2-22" \markup\wordwrap {
  Chorus: \italic { Hallelujah }
}
\includeScore "part2/22-chorus-Hallelujah"
\actEnd \markup { END OF THE SECOND PART }
\newBookPart #'()

\part "Part III"
%% n°4
\pieceTocNb "3-4" \markup\wordwrap {
  Air: \italic { The trumpet shall sound } (bass)
}
\includeScore "part3/4-air-TheTrumpeShallSound"
%% n°9
\pieceTocNb "3-9" \markup\wordwrap {
  Chorus: \italic { Worthy is the Lamb }
}
\includeScore "part3/9-chorus-WorthyIsTheLamb"
%% n°10
\pieceTocNb "3-10" \markup\wordwrap { Chorus: \italic { Amen } }
\includeScore "part3/10-chorus-Amen"
\actEnd \markup { END OF THE ORATORIO }
